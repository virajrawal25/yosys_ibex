// Copyright lowRISC contributors.
// Copyright 2018 ETH Zurich and University of Bologna, see also CREDITS.md.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

/**
 * Fetch Fifo for 32 bit memory interface
 *
 * input port: send address and data to the FIFO
 * clear_i clears the FIFO for the following cycle, including any new request
 */


import fv_pkg::*;

module fv_ibex_core_decode_if  import ibex_pkg::*; #(
    parameter int unsigned ADDR_WIDTH        = 5,
    parameter int unsigned NUM_WORDS         = 32,
    parameter int unsigned RegFileDataWidth  = 32
) (

    input  logic                                        clk_i,
    input  logic                                        rst_ni,
       
    input  logic                                        id_instr_valid_i,
    input  logic [INST_W-1:0]                           id_instr_rdata_i,
    input  logic                                        id_instr_ready_o_r,
    input  logic                                        id_instr_executing,// add modeling logic for the same 
    input  logic  [31:0]                                pc_if,
    input  logic  [31:0]                                pc_id,
    input  logic [NUM_WORDS-1:1][RegFileDataWidth-1:0]  reg_file_reg_q,
    output logic  [4:0]                                 return_address_pointer_restore                               
);


parameter NB_ELEM = 64;
parameter INDEX_WIDTH = NB_ELEM <= 7 ? 4 : NB_ELEM<= 15 ? 5 : NB_ELEM <32 ? 6 : NB_ELEM < 64 ? 7 : 8;
parameter DATA_WIDTH = 5;

wire [11:0] JUMP_ADDR=id_instr_rdata_i[31:20];
wire [INST_OPCODE_W-1:0] instr_opcode=id_instr_rdata_i[INST_OPCODE+:INST_OPCODE_W];
wire [4:0]           r1,r2,rd;//registers pointers
assign r1=
((instr_opcode==OP_OPCODE) ||
(instr_opcode==OP_IMM_OPCODE) ||
(instr_opcode==BRANCH_OPCODE) ||
(instr_opcode==JALR_OPCODE) ||
(instr_opcode==LOAD_OPCODE) ||
(instr_opcode==STORE_OPCODE) ||
(instr_opcode==MISC_MEM_OPCODE))?id_instr_rdata_i[19:15]:0;

assign r2=
((instr_opcode==OP_OPCODE) ||
(instr_opcode==BRANCH_OPCODE) ||
(instr_opcode==STORE_OPCODE))?id_instr_rdata_i[24:20]:0;

assign rd=
((instr_opcode==OP_OPCODE) ||
(instr_opcode==OP_IMM_OPCODE) ||
(instr_opcode==LUI_OPCODE) ||
(instr_opcode==AUIPC_OPCODE) ||
(instr_opcode==JAL_OPCODE) ||
(instr_opcode==JALR_OPCODE) ||
(instr_opcode==LOAD_OPCODE) ||
(instr_opcode==MISC_MEM_OPCODE))?id_instr_rdata_i[11:7]:0;

wire jump_request_JAL=id_instr_valid_i && id_instr_ready_o_r && id_instr_executing && (instr_opcode==JAL_OPCODE);
wire return_request_JALR=id_instr_valid_i && id_instr_ready_o_r && id_instr_executing && (instr_opcode==JALR_OPCODE);

wire [4:0] return_address_pointer_save = (jump_request_JAL)?rd:0;
wire [INDEX_WIDTH-1:0] num_outstading_jump;

/*
branch_return_address_handler return_handle_fifo (
    .clk           (clk_i)
    ,.rst_n         (rst_ni)
    ,.jump_req      (jump_request_JAL)
    ,.data_in       (return_address_pointer_save)
    ,.return_req    (return_request_JALR)
   
    ,.data_out      (return_address_pointer_restore)
    ,.empty         (fifo_empty)
    ,.full          (fifo_full)
    ,.jump_req_cnt  (num_outstading_jump)

);
*/

reg [4:0] return_address_pointer_restore_r;
always @(posedge clk_i) begin : always_return_address_r
    if (!rst_ni)
        return_address_pointer_restore_r<=0; 
    else if(return_request_JALR)
    	return_address_pointer_restore_r<=return_address_pointer_restore;
end

reg [NUM_WORDS-1:1] secure_reg;
always @(posedge clk_i) begin : secure_register
    if (!rst_ni)
        secure_reg<= 0;
    //Assigning security to register
    else if(jump_request_JAL)
    	secure_reg[return_address_pointer_save]<=1'b1;
    //revoking security from register
    else if(return_request_JALR)
    	secure_reg[return_address_pointer_restore]<=1'b0;
end

reg [NUM_WORDS-1:1] secure_reg1,secure_reg2;
always @(posedge clk_i) begin : secure_register_delay
    if (!rst_ni) begin
        secure_reg1 <= 0;
        secure_reg2 <= 0;
    end
    else begin
        secure_reg1 <= secure_reg;
        secure_reg2 <= secure_reg1;
    end
end


`ifndef YOSYS

genvar i;
generate 
for(i=1; i<32;i=i+1) begin
prop_inst_id_secure_registers_content_should_not_be_altered_by_another_instruction: assert property(`FV_CLKRESET
(id_instr_valid_i && id_instr_ready_o_r && id_instr_executing && secure_reg[i]==1 |->   rd!=i)
);	
end
endgenerate


wire [RegFileDataWidth-1:0] return_address= {reg_file_reg_q[return_address_pointer_restore_r][31:1],1'b0};

//PS: destination register can not be zero register as it will have no impact and X0 will always outputs zero irrespective of operation we do on it
prop_inst_id_no_X0_as_destination_reg: assert property(`FV_CLKRESET
(id_instr_valid_i |-> rd != 0)
);


prop_inst_id_jump_followed_by_return_with_previous_return_address: assert property(`FV_CLKRESET
(return_request_JALR |-> ((JUMP_ADDR==0) && r1==return_address_pointer_restore))
);

prop_inst_id_return_request_expect_non_zero_jump_request_outstanding: assert property(`FV_CLKRESET
(return_request_JALR |-> (num_outstading_jump!=0))
);


prop_inst_id_check_program_counter_should_be_updated_as_expected_for_JALR: assert property(`FV_CLKRESET
(return_request_JALR  |=> (pc_if==return_address))
);
`endif

endmodule
