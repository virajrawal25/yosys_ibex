// Copyright lowRISC contributors.
// Copyright 2018 ETH Zurich and University of Bologna, see also CREDITS.md.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0





/**
 * Formal verification instruction memory interface
 */
module fv_ibex_core_inst_if import ibex_pkg::*; #(
   parameter bit                 PMPEnable        = 1'b0,
    parameter int unsigned        PMPGranularity   = 0,
    parameter int unsigned        PMPNumRegions    = 4,
    parameter int unsigned        MHPMCounterNum   = 0,
    parameter int unsigned        MHPMCounterWidth = 40,
    parameter bit                 RV32E            = 1'b0,
    parameter ibex_pkg::rv32m_e   RV32M            = ibex_pkg::RV32MFast,
    parameter ibex_pkg::rv32b_e   RV32B            = ibex_pkg::RV32BNone,
    parameter ibex_pkg::regfile_e RegFile          = ibex_pkg::RegFileFF,
    parameter bit                 BranchTargetALU  = 1'b0,
    parameter bit                 WritebackStage   = 1'b0,
    parameter bit                 ICache           = 1'b0,
    parameter bit                 ICacheECC        = 1'b0,
    parameter bit                 BranchPredictor  = 1'b0,
    parameter bit                 DbgTriggerEn     = 1'b0,
    parameter int unsigned        DbgHwBreakNum    = 1,
    parameter bit                 SecureIbex       = 1'b0,
    parameter int unsigned        DmHaltAddr       = 32'h1A110800,
    parameter int unsigned        DmExceptionAddr  = 32'h1A110808

) (
    // Clock and Reset
    input  logic                         clk_i,
    input  logic                         rst_ni,
    
    input  logic                         fetch_enable_i,
    // Instruction memory interface
    input   logic                         instr_req_o,
    input   logic                         instr_gnt_i,
    input   logic                         instr_rvalid_i,
    input   logic [31:0]                  instr_addr_o,
    input   logic [31:0]                  instr_rdata_i,
    input   logic                         instr_err_i,
    
    input   logic                         ready_i,
    output  logic                         valid_o,
    output  logic [31:0]                  rdata_o,
    output  logic [31:0]                  addr_o,
    output  logic                         err_o,
    output  logic                         err_plus2_o,

    input   logic                         branch_i,
    input   logic                         branch_spec_i,
    input   logic                         predicted_branch_i,
    input   logic                         branch_mispredict_i,
    input   logic [31:0]                  addr_i,

    input   logic [31:0]                  pc_if,
    input   logic [31:0]                  pc_id,

    //pre_fetch_fifo
    // control signals
    input logic                         pre_fifo_clear_i,   // clears the contents of the FIFO
    input logic [NUM_REQS-1:0]          pre_fifo_busy_o,


    input logic                         pre_fifo_in_valid_i,
    input logic [31:0]                  pre_fifo_in_addr_i,
    input logic [31:0]                  pre_fifo_in_rdata_i,
    input logic                         pre_fifo_in_err_i,
    input logic [DEPTH-1:0] [31:0]      pre_fifo_rdata_q,
    input logic                         pre_fifo_pop_fifo,
    input logic [DEPTH-1:0]             pre_fifo_entry_en,
    input logic [DEPTH-1:0]             pre_fifo_valid_q,
    

    input logic                         pre_fifo_out_valid_o,
    input logic                         pre_fifo_out_ready_i,
    input logic [31:0]                  pre_fifo_out_addr_o,
    input logic [31:0]                  pre_fifo_out_addr_next_o,
    input logic [31:0]                  pre_fifo_out_rdata_o,
    input logic                         pre_fifo_out_err_o,
    input logic                         pre_fifo_out_err_plus2_o,




    output  reg                           my_fetch_req_rcvd,
    input [INST_OPCODE_W-1:0]            inst_opcode,
    input [INST_FUNC3_W-1:0]             inst_func3,
    input [INST_FUNC7_W-1:0]             inst_func7


  );


wire [31:0] IMMEDIATE,JUMP_ADDR;
wire [4:0]           r1,r2,rd;//registers pointers
wire [6:0] SHIFT_TYPE;

wire [INST_W-1:0]   inst_op_param          =(inst_opcode==OP_OPCODE)?{inst_func7,r2,r1,inst_func3,rd,inst_opcode}:0;
wire [INST_W-1:0]   inst_imm_param         =((inst_opcode==OP_IMM_OPCODE) && ((inst_func3 != op_imm_func3_SLLI) && 
                                             (inst_func3 != op_imm_func3_SRLI) && (inst_func3 != op_imm_func3_SRAI)))?{IMMEDIATE[11:0],r1,inst_func3,rd,inst_opcode}:0;
wire [INST_W-1:0]   inst_imm_shift_param   =((inst_opcode==OP_IMM_OPCODE) && ((inst_func3 == op_imm_func3_SLLI) || 
                                             (inst_func3 == op_imm_func3_SRLI) || (inst_func3 == op_imm_func3_SRAI)))?{SHIFT_TYPE,IMMEDIATE[4:0],r1,inst_func3,rd,inst_opcode}:0;
wire [INST_W-1:0]   inst_branch_param      =(inst_opcode==BRANCH_OPCODE)?{JUMP_ADDR[12],JUMP_ADDR[10:5],r2,r1,inst_func3,JUMP_ADDR[4:1],JUMP_ADDR[11],inst_opcode}:0;
wire [INST_W-1:0]   inst_LUI_AUIPC_param   =((inst_opcode==LUI_OPCODE) || (inst_opcode==AUIPC_OPCODE))?{IMMEDIATE[31:12],rd,inst_opcode}:0; 
wire [INST_W-1:0]   inst_JAL_param         =(inst_opcode==JAL_OPCODE)?{JUMP_ADDR[20],JUMP_ADDR[10:1],JUMP_ADDR[11],JUMP_ADDR[19:12],rd,inst_opcode}:0; 
wire [INST_W-1:0]   inst_JALR_param        =(inst_opcode==JALR_OPCODE)?{JUMP_ADDR[11:0],r1,inst_func3,rd,inst_opcode}:0;
wire [INST_W-1:0]   inst_LOAD_param        =(inst_opcode==LOAD_OPCODE)?{IMMEDIATE[11:0],r1,inst_func3,rd,inst_opcode}:0;
wire [INST_W-1:0]   inst_STORE_param       =(inst_opcode==STORE_OPCODE)?{IMMEDIATE[11:5],r2,r1,inst_func3,IMMEDIATE[4:0],inst_opcode}:0;
wire [INST_W-1:0]   inst_MISC_param        =0;   //=(inst_opcode==MISC_MEM_OPCODE)?{FM,PI,PO,PR,PW,SI,SO,SR,SW,r1,inst_func3,rd,inst_opcode}:0;

wire [INST_W-1:0] inst_param = inst_op_param | inst_imm_param | inst_imm_shift_param | inst_branch_param | inst_LUI_AUIPC_param | inst_JAL_param | inst_JALR_param | inst_LOAD_param | inst_STORE_param | inst_MISC_param;

prop_inst_if_instruction_data: assert property (`FV_CLKRESET
(instr_rdata_i == inst_param )
);




logic                branch_suppress;
logic                valid_new_req, valid_req;
logic                valid_req_d, valid_req_q;
logic                discard_req_d, discard_req_q;
logic                gnt_or_pmp_err, rvalid_or_pmp_err;
logic [NUM_REQS-1:0] rdata_outstanding_n, rdata_outstanding_s, rdata_outstanding_q;
logic [NUM_REQS-1:0] branch_discard_n, branch_discard_s, branch_discard_q;
logic [NUM_REQS-1:0] rdata_pmp_err_n, rdata_pmp_err_s, rdata_pmp_err_q;
logic [NUM_REQS-1:0] rdata_outstanding_rev;


logic                valid_raw;
logic [31:0]         addr_next;
logic [31:0]         branch_mispredict_addr;
logic [31:0]         instr_addr, instr_addr_w_aligned;

logic                branch_or_mispredict;
logic                fifo_valid;
logic [31:0]         fifo_addr;
logic                fifo_ready;
logic                fifo_clear;
logic [NUM_REQS-1:0] fifo_busy;


assign instr_addr_w_aligned = {instr_addr[31:2], 2'b00};

`ifdef UNALLIGNED_JUMP_OFF
prop_inst_if_unalligned_jump_address_disabled: assert property (`FV_CLKRESET
(JUMP_ADDR[1:0] == 2'b00 )
);
`endif

 


 //////////////
  // Requests //
  //////////////



  // Suppress a new request on a not-taken branch (as the external address will be incorrect)
  assign branch_suppress = branch_spec_i & ~branch_i;


// Make a new request any time there is space in the FIFO, and space in the request queue
  assign valid_new_req = ~branch_suppress & instr_req_o & fetch_enable_i & (fifo_ready | branch_or_mispredict) &
                         ~rdata_outstanding_q[NUM_REQS-1];

  assign valid_req = valid_new_req;


  // If a request address triggers a PMP error, the external bus request is suppressed. We might
  // therefore never receive a grant for such a request. The grant is faked in this case to make
  // sure the request proceeds and the error is pushed to the FIFO.
  assign gnt_or_pmp_err = instr_gnt_i; // | instr_pmp_err_i; //ignoring pmp errors for now

  // As with the grant, the rvalid must be faked for a PMP error, since the request was suppressed.
  assign rvalid_or_pmp_err = rdata_outstanding_q[0] & (instr_rvalid_i); // | rdata_pmp_err_q[0]);  //ignore physical memory protection error: disabled for now


  // Hold the request stable for requests that didn't get granted
//FV:  assign valid_req_d = valid_req & ~gnt_or_pmp_err;

  // Record whether an outstanding bus request is cancelled by a branch
//FV:  assign discard_req_d = valid_req_q & (branch_or_mispredict | discard_req_q);

 ////////////////////////////
  // Prefetch buffer status //
  ////////////////////////////

  assign busy_o = (|rdata_outstanding_q) | instr_req_o;

  assign branch_or_mispredict = branch_i | branch_mispredict_i;

  //////////////////////////////////////////////
  // Fetch fifo - consumes addresses and data //
  //////////////////////////////////////////////

  // Instruction fetch errors are valid on the data phase of a request
  // PMP errors are generated in the address phase, and registered into a fake data phase
  assign instr_or_pmp_err = instr_err_i ; // | rdata_pmp_err_q[0]);  //ignore physical memory protection error: disabled for now

  // A branch will invalidate any previously fetched instructions.
  // Note that the FENCE.I instruction relies on this flushing behaviour on branch. If it is
  // altered the FENCE.I implementation may require changes.
  assign fifo_clear = branch_or_mispredict;

  // Reversed version of rdata_outstanding_q which can be overlaid with fifo fill state
  for (genvar i = 0; i < NUM_REQS; i++) begin : gen_rd_rev
    assign rdata_outstanding_rev[i] = rdata_outstanding_q[NUM_REQS-1-i];
  end

  // The fifo is ready to accept a new request if it is not full - including space reserved for
  // requests already outstanding.
  // Overlay the fifo fill state with the outstanding requests to see if there is space.
  assign fifo_ready = ~&(fifo_busy | rdata_outstanding_rev);



 
///////////////////////////////
  // Request outstanding queue //
  ///////////////////////////////

  for (genvar i = 0; i < NUM_REQS; i++) begin : g_outstanding_reqs
    // Request 0 (always the oldest outstanding request)
    if (i == 0) begin : g_req0
      // A request becomes outstanding once granted, and is cleared once the rvalid is received.
      // Outstanding requests shift down the queue towards entry 0.
      assign rdata_outstanding_n[i] = (valid_req & gnt_or_pmp_err) |
                                      rdata_outstanding_q[i];
//FV:      // If a branch is received at any point while a request is outstanding, it must be tracked
//FV:      // to ensure we discard the data once received
//FV:      assign branch_discard_n[i]    = (valid_req & gnt_or_pmp_err & discard_req_d) |
//FV:                                      (branch_or_mispredict & rdata_outstanding_q[i]) |
//FV:                                      branch_discard_q[i];
//FV:      // Record whether this request received a PMP error
//FV:      assign rdata_pmp_err_n[i]     = (valid_req & ~rdata_outstanding_q[i] & instr_pmp_err_i) |
//FV:                                     rdata_pmp_err_q[i];

    end else begin : g_reqtop
    // Entries > 0 consider the FIFO fill state to calculate their next state (by checking
    // whether the previous entry is valid)

      assign rdata_outstanding_n[i] = (valid_req & gnt_or_pmp_err &
                                       rdata_outstanding_q[i-1]) |
                                      rdata_outstanding_q[i];
//      assign branch_discard_n[i]    = (valid_req & gnt_or_pmp_err & discard_req_d &
//                                       rdata_outstanding_q[i-1]) |
//                                      (branch_or_mispredict & rdata_outstanding_q[i]) |
//                                      branch_discard_q[i];
//      assign rdata_pmp_err_n[i]     = (valid_req & ~rdata_outstanding_q[i] & instr_pmp_err_i &
//                                       rdata_outstanding_q[i-1]) |
//                                      rdata_pmp_err_q[i];
    end
  end

  // Shift the entries down on each instr_rvalid_i
  assign rdata_outstanding_s = rvalid_or_pmp_err ? {1'b0,rdata_outstanding_n[NUM_REQS-1:1]} :
                                                   rdata_outstanding_n;
 //FV: assign branch_discard_s    = rvalid_or_pmp_err ? {1'b0,branch_discard_n[NUM_REQS-1:1]} :
 //FV:                                                  branch_discard_n;
 //FV: assign rdata_pmp_err_s     = rvalid_or_pmp_err ? {1'b0,rdata_pmp_err_n[NUM_REQS-1:1]} :
 //FV:                                                  rdata_pmp_err_n;

  // Push a new entry to the FIFO once complete (and not cancelled by a branch)
  assign fifo_valid = rvalid_or_pmp_err; //& ~branch_discard_q[0]; // ignore branch prediction

  assign fifo_addr = branch_i ? addr_i : branch_mispredict_addr;

  ///////////////
  // Registers //
  ///////////////

  always_ff @(posedge clk_i or negedge rst_ni) begin
    if (!rst_ni) begin
      valid_req_q          <= 1'b0;
    //FV:  discard_req_q        <= 1'b0;
      rdata_outstanding_q  <= 'b0;
    //FV:  branch_discard_q     <= 'b0;
    //FV:  rdata_pmp_err_q      <= 'b0;
    end else begin
      valid_req_q          <= valid_req_d;
    //FV:  discard_req_q        <= discard_req_d;
      rdata_outstanding_q  <= rdata_outstanding_s;
    //FV:  branch_discard_q     <= branch_discard_s;
    //FV:  rdata_pmp_err_q      <= rdata_pmp_err_s;
    end
  end


`ifndef YOSYS
prop_inst_if_legel_opcodes: assert property (`FV_CLKRESET
(
`ifdef OP
inst_opcode==OP_OPCODE
`endif //OP
`ifdef OP_IMM
|| inst_opcode==OP_IMM_OPCODE 
`endif //OP_IMM
`ifdef BRANCH
|| inst_opcode==BRANCH_OPCODE 
`endif //BRANCH
`ifdef LUI
|| inst_opcode==LUI_OPCODE 
`endif //LUI
`ifdef AUIPC
|| inst_opcode==AUIPC_OPCODE 
`endif //AUIPC
`ifdef JAL
|| inst_opcode==JAL_OPCODE 
`endif //JAL
`ifdef JALR
|| inst_opcode==JALR_OPCODE 
`endif //JALR
`ifdef LOAD
|| inst_opcode==LOAD_OPCODE 
`endif //LOAD
`ifdef STORE
|| inst_opcode==STORE_OPCODE 
`endif //STORE
`ifdef MISC
|| inst_opcode==MISC_MEM_OPCODE
`endif //MISC
)
);


prop_inst_if_legel_func3_func7_OP: assert property (`FV_CLKRESET
(inst_opcode == OP_OPCODE   |-> 
((inst_func3 ==op_func3_ADD  && inst_func7 ==op_func7_ADD ) ||
(inst_func3 ==op_func3_SUB  && inst_func7 ==op_func7_SUB ) ||
(inst_func3 ==op_func3_SLL  && inst_func7 ==op_func7_SLL ) ||
(inst_func3 ==op_func3_SLT  && inst_func7 ==op_func7_SLT ) ||
(inst_func3 ==op_func3_SLTU && inst_func7 ==op_func7_SLTU) ||
(inst_func3 ==op_func3_XOR  && inst_func7 ==op_func7_XOR ) ||
(inst_func3 ==op_func3_SRL  && inst_func7 ==op_func7_SRL ) ||
(inst_func3 ==op_func3_SRA  && inst_func7 ==op_func7_SRA ) ||
(inst_func3 ==op_func3_OR   && inst_func7 ==op_func7_OR  ) ||
(inst_func3 ==op_func3_AND  && inst_func7 ==op_func7_AND )))
);



prop_inst_if_legel_func3_OP_IMM: assert property (`FV_CLKRESET
(inst_opcode == OP_IMM_OPCODE   |-> 
(inst_func3 ==op_imm_func3_ADDI  ||
inst_func3 ==op_imm_func3_SLTI  ||
inst_func3 ==op_imm_func3_SLTIU  ||
inst_func3 ==op_imm_func3_XORI  ||
inst_func3 ==op_imm_func3_ORI  ||
inst_func3 ==op_imm_func3_ANDI ||
inst_func3 ==op_imm_func3_SLLI  ||
inst_func3 ==op_imm_func3_SRLI  ||
inst_func3 ==op_imm_func3_SRAI ))
);


prop_inst_if_legel_func3_BRANCH: assert property (`FV_CLKRESET
(inst_opcode == BRANCH_OPCODE   |->  
(inst_func3 ==branch_func3_BEQ  ||
inst_func3 ==branch_func3_BNE  ||
inst_func3 ==branch_func3_BLT  ||
inst_func3 ==branch_func3_BGE  ||
inst_func3 ==branch_func3_BLTU ||
inst_func3 ==branch_func3_BGEU ))
);

prop_inst_if_legel_func3_JALR: assert property (`FV_CLKRESET
(inst_opcode == JALR_OPCODE   |-> inst_func3 ==jalr_func3)
);




prop_inst_if_legel_func3_LOAD: assert property (`FV_CLKRESET
(inst_opcode == LOAD_OPCODE   |->  
(inst_func3 ==load_func3_LB  ||
inst_func3 ==load_func3_LH  ||
inst_func3 ==load_func3_LW  ||
inst_func3 ==load_func3_LBU  ||
inst_func3 ==load_func3_LHU ))
);


prop_inst_if_legel_func3_STORE: assert property (`FV_CLKRESET
(inst_opcode == STORE_OPCODE   |->   
(inst_func3 ==store_func3_SB  ||
inst_func3 ==store_func3_SH  ||
inst_func3 ==store_func3_SW ))
);

//Not supported in design
//inst_func3 ==store_func3_SBU  ||
//inst_func3 ==store_func3_SHU 


prop_inst_if_legel_func3_MISC_MEM: assert property (`FV_CLKRESET
(inst_opcode == MISC_MEM_OPCODE   |->  
(inst_func3 ==misc_mem_func3_FENCE  ||
inst_func3 ==misc_mem_func3_FENCE_I))
);

prop_inst_if_legel_SHIFT_TYPE_IMM_SHIFT: assert property (`FV_CLKRESET
(inst_opcode == OP_IMM_OPCODE   |->  
(SHIFT_TYPE ==SHIFT_TYPE_LOGIC  ||
 inst_func3 == op_imm_func3_SRAI && SHIFT_TYPE ==SHIFT_TYPE_ARITH))
);


reg my_fetch_valid;
always @(posedge clk_i) begin
    if(~rst_ni)
    	my_fetch_valid <=1'b0; 
   else if(instr_req_o && instr_rvalid_i && instr_gnt_i)
    	my_fetch_valid <=1'b1;
   else
    	my_fetch_valid <=1'b0;
end



always @(posedge clk_i) begin
    if(~rst_ni)
    	my_fetch_req_rcvd =1'b0; 
   else if(instr_req_o && instr_rvalid_i && instr_gnt_i)
    	my_fetch_req_rcvd =1'b1;
   else
    	my_fetch_req_rcvd =1'b0;
end


/*
reg [4:0] r;
reg jump2return;
always @(posedge clk_i) begin
    if(~rst_ni)
    	jump2return =1'b0; 
   else if(my_fetch_valid && (inst_opcode==JAL_OPCODE)) begin
    	jump2return =1'b1;
    	r=rd;
    	end
   else if(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE))
    	jump2return =1'b0;
end

//prop_inst_if_jump_followed_by_return: assert property(`FV_CLKRESET
//(jump2return |-> s_eventually (instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE)))
//);

prop_inst_if_check_JAL_followed_by_JALR_cover: cover property(`FV_CLKRESET
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JAL_OPCODE)) ##1 
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE)) ##1
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JAL_OPCODE)) ##1 
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE)) ##1
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JAL_OPCODE)) ##1 
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE))
);


prop_inst_if_JALR_followed_by_JAL: assert property(`FV_CLKRESET
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE)) |=> (instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JAL_OPCODE)) 
);

prop_inst_if_JAL_followed_by_JALR: assert property(`FV_CLKRESET
(instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JAL_OPCODE)) |=> (instr_req_o && instr_rvalid_i && instr_gnt_i && (inst_opcode==JALR_OPCODE)) 
);


*/
prop_inst_if_if_req_should_last_for_two_clock_cycle_min: assert property(`FV_CLKRESET
(instr_req_o |-> instr_req_o ##[2:$] !instr_req_o )
);


prop_inst_if_if_req_then_valid: assert property(`FV_CLKRESET
(instr_req_o |->  instr_rvalid_i && instr_gnt_i)
);

prop_inst_if_no_req_no_valid: assert property(`FV_CLKRESET
(!instr_req_o |-> !instr_rvalid_i && !instr_gnt_i)
);

/*
reg pre_fetch_fifo_full;
reg [5:0] fifo_emt_cnt; 
always @(posedge clk_i)
begin
    if(~rst_ni) begin
    	pre_fetch_fifo_full =1'b0;
    	fifo_emt_cnt =DEPTH;
   end
   else if(instr_req_o && instr_rvalid_i && instr_gnt_i) begin
   	   if(fifo_emt_cnt>0)
   	   fifo_emt_cnt=fifo_emt_cnt-1;
   	   
   	   if(fifo_emt_cnt==0)
    	pre_fetch_fifo_full =1'b1; 
    end
end



prop_inst_if_fifo_full_no_req: assert property(`FV_CLKRESET
(pre_fetch_fifo_full |-> !instr_req_o )
);


prop_inst_if_when_valid_params_should_be_table: assert property(`FV_CLKRESET
(instr_rvalid_i |-> $stable(inst_opcode) && $stable(inst_func7) && $stable(inst_func3))
);
*/





prop_inst_if_no_error: assert property(`FV_CLKRESET
(!instr_err_i)
);


//PS: If no branch prediction then no predict signals should be asserted
prop_inst_if_disabled_branch_prediction: assert property (`FV_CLKRESET
(BranchPredictor == 0 |-> !branch_i && !branch_spec_i && !predicted_branch_i && !branch_mispredict_i)
);




//checkers
//PS: if req has come then valid should be asserted high eventually 

prop_inst_if_check_if_req_eventually_valid: assert property (`FV_CLKRESET
(instr_req_o |-> s_eventually my_fetch_req_rcvd )
);


//PS: If no branch prediction then fetch address should be word alligned
prop_inst_if_check_addr_should_be_word_allign_if_no_branch_prediction_with_valid_req: assert property (`FV_CLKRESET
(BranchPredictor == 0 && my_fetch_req_rcvd |-> instr_addr_o == $past(instr_addr_o+4) )
);
`endif


fv_ibex_pre_fetch_fifo #(
        .NUM_REQS (NUM_REQS),
        .ResetAll (1'b0)
    ) fv_pre_fetch_fifo_i (
        .clk_i(clk_i),
        .rst_ni(rst_ni),
        .pre_fifo_clear_i(pre_fifo_clear_i),
        .pre_fifo_busy_o(pre_fifo_busy_o),
        .pre_fifo_in_valid_i(pre_fifo_in_valid_i),
        .pre_fifo_in_addr_i(pre_fifo_in_addr_i),
        .pre_fifo_in_rdata_i(pre_fifo_in_rdata_i),
        .pre_fifo_in_err_i(pre_fifo_in_err_i),
        .pre_fifo_rdata_q(pre_fifo_rdata_q),
        .pre_fifo_out_valid_o(pre_fifo_out_valid_o),
        .pre_fifo_out_ready_i(pre_fifo_out_ready_i),
        .pre_fifo_out_addr_o(pre_fifo_out_addr_o),
        .pre_fifo_out_addr_next_o(pre_fifo_out_addr_next_o),
        .pre_fifo_out_rdata_o(pre_fifo_out_rdata_o),
        .pre_fifo_out_err_o(pre_fifo_out_err_o),
        .pre_fifo_out_err_plus2_o(pre_fifo_out_err_plus2_o),
        .pre_fifo_pop_fifo(pre_fifo_pop_fifo),
        .pre_fifo_entry_en(pre_fifo_entry_en),
        .pre_fifo_valid_q(pre_fifo_valid_q)
    );
  


 /////////////
  // Outputs //
  /////////////

  assign valid_o = valid_raw & ~branch_mispredict_i;



endmodule

