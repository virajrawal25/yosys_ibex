// Copyright lowRISC contributors.
// Copyright 2018 ETH Zurich and University of Bologna, see also CREDITS.md.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0



/**
 * Top level module of the ibex RISC-V core fetch unit formal test bench
 */

module fv_ibex_core_top_tb import ibex_pkg::*; #(
    parameter bit                 PMPEnable        = 1'b0,
    parameter int unsigned        PMPGranularity   = 0,
    parameter int unsigned        PMPNumRegions    = 4,
    parameter int unsigned        MHPMCounterNum   = 0,
    parameter int unsigned        MHPMCounterWidth = 40,
    parameter bit                 RV32E            = 1'b0,
    parameter ibex_pkg::rv32m_e   RV32M            = ibex_pkg::RV32MFast,
    parameter ibex_pkg::rv32b_e   RV32B            = ibex_pkg::RV32BNone,
    parameter ibex_pkg::regfile_e RegFile          = ibex_pkg::RegFileFF,
    parameter bit                 BranchTargetALU  = 1'b0,
    parameter bit                 WritebackStage   = 1'b0,
    parameter bit                 ICache           = 1'b0,
    parameter bit                 ICacheECC        = 1'b0,
    parameter bit                 BranchPredictor  = 1'b0,
    parameter bit                 DbgTriggerEn     = 1'b0,
    parameter int unsigned        DbgHwBreakNum    = 1,
    parameter bit                 SecureIbex       = 1'b0,
    parameter int unsigned        DmHaltAddr       = 32'h1A110800,
    parameter int unsigned        DmExceptionAddr  = 32'h1A110808,
    parameter int unsigned        RegFileDataWidth = 32,
    parameter int unsigned        ADDR_WIDTH       = RV32E ? 4 : 5,
    parameter int unsigned        NUM_WORDS        = 2**ADDR_WIDTH


) (
    // Clock and Reset
    input  logic                         clk_i,
    input  logic                         rst_ni,

    input  logic                         test_en_i,     // enable all clock gates for testing
    input  prim_ram_1p_pkg::ram_1p_cfg_t ram_cfg_i,

    input  logic [31:0]                  hart_id_i,
    input  logic [31:0]                  boot_addr_i,

    // Instruction memory interface
    input logic                         instr_req_o,
    input  logic                         instr_gnt_i,
    input  logic                         instr_rvalid_i,
    input logic [31:0]                  instr_addr_o,
    input  logic [31:0]                  instr_rdata_i,
    input  logic                         instr_err_i,

    // Data memory interface
    input logic                         data_req_o,
    input  logic                         data_gnt_i,
    input  logic                         data_rvalid_i,
    input logic                         data_we_o,
    input logic [3:0]                   data_be_o,
    input logic [31:0]                  data_addr_o,
    input logic [31:0]                  data_wdata_o,
    input  logic [31:0]                  data_rdata_i,
    input  logic                         data_err_i,

    // Interrupt inputs
    input  logic                         irq_software_i,
    input  logic                         irq_timer_i,
    input  logic                         irq_external_i,
    input  logic [14:0]                  irq_fast_i,
    input  logic                         irq_nm_i,       // non-maskeable interrupt

    // Debug Interface
    input  logic                         debug_req_i,
    input ibex_pkg::crash_dump_t        crash_dump_o,

    // RISC-V Formal Interface
    // Does not comply with the coding standards of _i/_o suffixes, but follows
    // the convention of RISC-V Formal Interface Specification.
`ifdef RVFI
    input logic                         rvfi_valid,
    input logic [63:0]                  rvfi_order,
    input logic [31:0]                  rvfi_insn,
    input logic                         rvfi_trap,
    input logic                         rvfi_halt,
    input logic                         rvfi_intr,
    input logic [ 1:0]                  rvfi_mode,
    input logic [ 1:0]                  rvfi_ixl,
    input logic [ 4:0]                  rvfi_rs1_addr,
    input logic [ 4:0]                  rvfi_rs2_addr,
    input logic [ 4:0]                  rvfi_rs3_addr,
    input logic [31:0]                  rvfi_rs1_rdata,
    input logic [31:0]                  rvfi_rs2_rdata,
    input logic [31:0]                  rvfi_rs3_rdata,
    input logic [ 4:0]                  rvfi_rd_addr,
    input logic [31:0]                  rvfi_rd_wdata,
    input logic [31:0]                  rvfi_pc_rdata,
    input logic [31:0]                  rvfi_pc_wdata,
    input logic [31:0]                  rvfi_mem_addr,
    input logic [ 3:0]                  rvfi_mem_rmask,
    input logic [ 3:0]                  rvfi_mem_wmask,
    input logic [31:0]                  rvfi_mem_rdata,
    input logic [31:0]                  rvfi_mem_wdata,
`endif

    // CPU Control Signals
    input  logic                         fetch_enable_i,
    input logic                         alert_minor_o,
    input logic                         alert_major_o,
    input logic                         core_sleep_o,

    // DFT bypass controls
    input logic                          scan_rst_ni,

    input  logic                id_instr_valid_i,
    input  logic [31:0]         id_instr_rdata_i,
    input  logic                id_instr_ready_o, 
    input  logic                id_instr_executing,
    input  logic [NUM_WORDS-1:1][RegFileDataWidth-1:0] reg_file_reg_q,

    input logic  [1:0]                  write_back_mux_write_en, 
    input logic  [31:0]                 pc_if,
    input logic  [31:0]                 pc_id,
    
    //pre_fetch_fifo
    // control signals
    input logic                         pre_fifo_clear_i,   // clears the contents of the FIFO
    input logic [NUM_REQS-1:0]          pre_fifo_busy_o,


    input logic                         pre_fifo_in_valid_i,
    input logic [31:0]                  pre_fifo_in_addr_i,
    input logic [31:0]                  pre_fifo_in_rdata_i,
    input logic                         pre_fifo_in_err_i,
    input logic [DEPTH-1:0] [31:0]      pre_fifo_rdata_q,
    input logic                         pre_fifo_pop_fifo,
    input logic [DEPTH-1:0]             pre_fifo_entry_en,
    input logic [DEPTH-1:0]             pre_fifo_valid_q,
    

    input logic                         pre_fifo_out_valid_o,
    input logic                         pre_fifo_out_ready_i,
    input logic [31:0]                  pre_fifo_out_addr_o,
    input logic [31:0]                  pre_fifo_out_addr_next_o,
    input logic [31:0]                  pre_fifo_out_rdata_o,
    input logic                         pre_fifo_out_err_o,
    input logic                         pre_fifo_out_err_plus2_o,


    input [INST_OPCODE_W-1:0]           inst_opcode,
    input [INST_FUNC3_W-1:0]            inst_func3,
    input [INST_FUNC7_W-1:0]            inst_func7
);

 

logic              fetch_valid;
logic              fetch_ready;
logic       [31:0] fetch_rdata;
logic       [31:0] fetch_addr;
logic              fetch_err;
logic              fetch_err_plus2;
reg                my_fetch_req_rcvd;
logic       [31:0] fetch_addr_n;
logic       [4:0]  return_address_pointer_restore;  
// fetch_enable_i can be used to stop the core fetching new instructions
//FV:  assign instr_req_gated = instr_req_int & fetch_enable_i;

assign branch_req  = 0; // pc_set_i | predict_branch_taken; //FV: Disable branch prediction
assign branch_spec = 0; // pc_set_spec_i | predict_branch_taken; //FV: Disable branch prediction

// The Branch predictor can provide a new PC which is internal to if_stage. Only override the mux
  // select to choose this if the core isn't already trying to set a PC.
  assign pc_mux_internal =
    (BranchPredictor); // && predict_branch_taken && !pc_set_i) ? PC_BP : pc_mux_i; // FV: Disabled branch prediction

 // fetch address selection mux
  always_comb begin : fetch_addr_mux
    unique case (pc_mux_internal)
      PC_BOOT: fetch_addr_n = { boot_addr_i[31:8], 8'h80 };
      //FV: Hardcoded for now
      PC_JUMP: fetch_addr_n = 0; //branch_target_ex_i;
      PC_EXC:  fetch_addr_n = 0; //exc_pc;                       // set PC to exception handler
      PC_ERET: fetch_addr_n = 0; //csr_mepc_i;                   // restore PC when returning from EXC
      PC_DRET: fetch_addr_n = 0; //csr_depc_i;
      // Without branch predictor will never get pc_mux_internal == PC_BP. We still handle no branch
      // predictor case here to ensure redundant mux logic isn't synthesised.
      PC_BP:   fetch_addr_n = { boot_addr_i[31:8], 8'h80 }; // BranchPredictor ? predict_branch_pc : { boot_addr_i[31:8], 8'h80 }; // FV: Disabled branch prediction
      default: fetch_addr_n = { boot_addr_i[31:8], 8'h80 };
    endcase
  end



fv_ibex_core_inst_if  #(
      .PMPEnable(PMPEnable),
    .PMPGranularity(PMPGranularity),
    .PMPNumRegions(PMPNumRegions),
    .MHPMCounterNum(MHPMCounterNum),
    .MHPMCounterWidth(MHPMCounterWidth),
    .RV32E(RV32E),
    .RV32M(RV32M),
    .RV32B(RV32B),
    .RegFile(RegFile),
    .BranchTargetALU(BranchTargetALU),
    .WritebackStage(WritebackStage),
    .ICache(ICache),
    .ICacheECC(ICacheECC),
    .BranchPredictor(BranchPredictor),
    .DbgTriggerEn(DbgTriggerEn),
    .DbgHwBreakNum(DbgHwBreakNum),
    .SecureIbex(SecureIbex),
    .DmHaltAddr(DmHaltAddr),
    .DmExceptionAddr(DmExceptionAddr)

)  instruction_if_checker (
        .clk_i                      ( clk_i                     )
        ,.rst_ni                    ( rst_ni                    )
        
        ,.fetch_enable_i            ( fetch_enable_i            )
        ,.instr_req_o               ( instr_req_o               )
        ,.instr_gnt_i               ( instr_gnt_i               )
        ,.instr_rvalid_i            ( instr_rvalid_i            )
        ,.instr_addr_o              ( instr_addr_o              )
        ,.instr_rdata_i             ( instr_rdata_i             )
        ,.instr_err_i               ( instr_err_i               )
        ,.inst_opcode               ( inst_opcode               )
        ,.inst_func3                ( inst_func3                )
        ,.inst_func7                ( inst_func7                )
        ,.my_fetch_req_rcvd         ( my_fetch_req_rcvd         ) 
        ,.ready_i                   ( 1'b1                      )   //fetch_ready       )//FV: hardcoded for now
        ,.valid_o                   ( fetch_valid               )
        ,.rdata_o                   ( fetch_rdata               )
        ,.addr_o                    ( fetch_addr                )
        ,.err_o                     ( fetch_err                 )
        ,.err_plus2_o               ( fetch_err_plus2           )
        ,.pc_if                     ( pc_if                     )
        ,.pc_id                     ( pc_id                     )

        ,.branch_i                  ( branch_req                )
        ,.branch_spec_i             ( branch_spec               )
        ,.predicted_branch_i        ( 1'b0                      ) //predicted_branch           ), //FV: Disable branch prediction
        ,.branch_mispredict_i       ( 1'b0                      ) //nt_branch_mispredict_i     ), //FV: Disable branch prediction
        ,.addr_i                    ( {fetch_addr_n[31:1], 1'b0})

        
        //pre_fetch_fifo
        ,.pre_fifo_clear_i          (  pre_fifo_clear_i         )  
        ,.pre_fifo_busy_o           (  pre_fifo_busy_o          )   
        ,.pre_fifo_in_valid_i       (  pre_fifo_in_valid_i      )
        ,.pre_fifo_in_addr_i        (  pre_fifo_in_addr_i       )
        ,.pre_fifo_in_rdata_i       (  pre_fifo_in_rdata_i      )
        ,.pre_fifo_in_err_i         (  pre_fifo_in_err_i        )
        ,.pre_fifo_rdata_q          (  pre_fifo_rdata_q         )
        ,.pre_fifo_out_valid_o      (  pre_fifo_out_valid_o     )
        ,.pre_fifo_out_ready_i      (  pre_fifo_out_ready_i     )
        ,.pre_fifo_out_addr_o       (  pre_fifo_out_addr_o      )
        ,.pre_fifo_out_addr_next_o  (  pre_fifo_out_addr_next_o )
        ,.pre_fifo_out_rdata_o      (  pre_fifo_out_rdata_o     )
        ,.pre_fifo_out_err_o        (  pre_fifo_out_err_o       )
        ,.pre_fifo_out_err_plus2_o  (  pre_fifo_out_err_plus2_o )
        ,.pre_fifo_pop_fifo         (  pre_fifo_pop_fifo        )
        ,.pre_fifo_entry_en         (  pre_fifo_entry_en        )
        ,.pre_fifo_valid_q          (  pre_fifo_valid_q         )
);



`ifndef YOSYS
prop_tb_top_no_intrrupt: assert property(`FV_CLKRESET
(!irq_software_i && !irq_timer_i && !irq_external_i && !irq_fast_i && !irq_nm_i));

prop_tb_top_debug_off:  assert property(`FV_CLK
    (debug_req_i==0)
);

prop_tb_top_clock_gating_off:  assert property(`FV_CLK
    (test_en_i==0)
);

reg fifo_valid;
reg [31:0] instr_data_r,instr_data_r_1; 
always @(posedge clk_i)
begin
    if(~rst_ni) 
    	instr_data_r <=0;
   else begin
  	    if(instr_req_o && instr_gnt_i && instr_rvalid_i && my_fetch_req_rcvd ) begin
   	        fifo_valid<=1'b1;
   	        instr_data_r_1 <= instr_rdata_i;
   	    end
   	    
   	    if(pre_fifo_out_ready_i) begin
   	        if(fifo_valid)
   	            instr_data_r <= instr_data_r_1;
   	        else
        		instr_data_r <= instr_rdata_i;
        end

    end
end



prop_tb_top_remain_stable:  assert property(`FV_CLK
   (debug_req_i==0) |=> $stable(debug_req_i) && $stable(boot_addr_i)
);

//PS: at power on reset state PC will be in PC_BOOT state and initial fetch address is set to 32'h00 which later will becomes 32'h80
prop_tb_top_starting_fetch_addr: assert property (`FV_CLK
    (boot_addr_i==0)
    );

prop_tb_top_if_no_req_then_no_valid_for_data_memory:  assert property(`FV_CLKRESET
!data_req_o |-> !data_rvalid_i && !data_gnt_i
);

prop_tb_top_check_requested_data_from_inst_memory_should_matches_with_input_to_decode:  assert property(`FV_CLKRESET
instr_req_o && instr_gnt_i && instr_rvalid_i && pre_fifo_out_ready_i && my_fetch_req_rcvd |=> ##1  (instr_data_r==id_instr_rdata_i)
);

//PS: Discuss with designer
prop_tb_top_one_hot_write_back_en:  assert property(`FV_CLKRESET
!(write_back_mux_write_en[0] && write_back_mux_write_en[1])
);
`endif
reg id_instr_ready_o_r;
always @(posedge clk_i) begin : always_ready_address_r
    if (!rst_ni) id_instr_ready_o_r<=0; 
    else id_instr_ready_o_r<=id_instr_ready_o;
end
 fv_ibex_core_decode_if # (
     .ADDR_WIDTH        ( ADDR_WIDTH      ),
     .NUM_WORDS         ( NUM_WORDS       ),
     .RegFileDataWidth  ( RegFileDataWidth)
  ) fv_ibex_decode_if (
      .clk_i(clk_i),
      .rst_ni(rst_ni),
      .id_instr_valid_i(id_instr_valid_i),
      .id_instr_rdata_i(id_instr_rdata_i),
      .id_instr_ready_o_r(id_instr_ready_o_r),
      .id_instr_executing(id_instr_executing),
      .pc_if(pc_if),
      .pc_id(pc_id),
      .reg_file_reg_q(reg_file_reg_q),
      .return_address_pointer_restore(return_address_pointer_restore)
  );
   


initial begin
    assume(rst);
  end

  always @* begin
    if (!$initstate) begin
      assume(instr_rvalid_i==1 & instr_gnt_i==1);
      assert(instr_rvalid_i==1 & instr_req_o==1);
    end
  end

endmodule
