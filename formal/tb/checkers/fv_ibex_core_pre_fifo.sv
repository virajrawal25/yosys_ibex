// Copyright lowRISC contributors.
// Copyright 2018 ETH Zurich and University of Bologna, see also CREDITS.md.
// Licensed under the Apache License, Version 2.0, see LICENSE for details.
// SPDX-License-Identifier: Apache-2.0

/**
 * Fetch Fifo for 32 bit memory interface
 *
 * input port: send address and data to the FIFO
 * clear_i clears the FIFO for the following cycle, including any new request
 */


module fv_ibex_pre_fetch_fifo import fv_pkg::*; #(
  parameter int unsigned NUM_REQS = 2,
  parameter bit          ResetAll = 1'b0
) (
    input  logic                clk_i,
    input  logic                rst_ni,

       
    input logic                         pre_fifo_clear_i,   // clears the contents of the FIFO
    input logic [NUM_REQS-1:0]          pre_fifo_busy_o,


    input logic                         pre_fifo_in_valid_i,
    input logic [31:0]                  pre_fifo_in_addr_i,
    input logic [31:0]                  pre_fifo_in_rdata_i,
    input logic                         pre_fifo_in_err_i,
    input logic [DEPTH-1:0] [31:0]      pre_fifo_rdata_q,
    

    input logic                         pre_fifo_out_valid_o,
    input logic                         pre_fifo_out_ready_i,
    input logic [31:0]                  pre_fifo_out_addr_o,
    input logic [31:0]                  pre_fifo_out_addr_next_o,
    input logic [31:0]                  pre_fifo_out_rdata_o,
    input logic                         pre_fifo_out_err_o,
    input logic                         pre_fifo_out_err_plus2_o,
    input logic                         pre_fifo_pop_fifo,
    input logic [DEPTH-1:0]             pre_fifo_entry_en,
    input logic [DEPTH-1:0]             pre_fifo_valid_q


);
`ifndef YOSYS
//Checkers

//PS: Must not push and pop simultaneously when FIFO full.
prop_pre_fifo_rtl_check_no_push_pop_simultaneously_when_fifo_full: assert property (`FV_CLKRESET
((pre_fifo_in_valid_i && pre_fifo_pop_fifo) |-> (!pre_fifo_valid_q[DEPTH-1] || pre_fifo_clear_i))
);

//PS: Must not push to FIFO when full.
prop_pre_fifo_rtl_check_no_push_when_fifo_full: assert property (`FV_CLKRESET
((pre_fifo_in_valid_i) |-> (!pre_fifo_valid_q[DEPTH-1] || pre_fifo_clear_i))
);

prop_pre_fifo_rtl_entry_en_0: cover property (`FV_CLKRESET
 pre_fifo_entry_en[0]  
);

prop_pre_fifo_rtl_entry_en_1: cover property (`FV_CLKRESET
 pre_fifo_entry_en[1]  
);

prop_pre_fifo_rtl_entry_en_2: cover property (`FV_CLKRESET
 pre_fifo_entry_en[2]  
);

`endif



endmodule
