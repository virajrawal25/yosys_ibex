set APP_MODE FPV

#Set the design top in a varidable
set top ibex_top


# Enable APP mode
# The values could be FPV COV AEP SEQ CC FTA
set_fml_appmode $APP_MODE

# Setup maximum run time for Formal
set_fml_var fml_max_time 24H

#restore the session if the job got terminated unexpectedly
set_app_var fml_auto_save true

# enable witness trace for vacuity 
set_fml_var fml_witness_on true

# enable viewing of reset + formal trace in one composite trace
set_app_var fml_composite_trace true

# Allow property check during reset to mark reset cover points as covered
set_fml_var fml_reset_property_check false


# FSm name auto extraction 
set_app_var fml_trace_auto_fsm_state_extraction true

# Verdi settings from Tcl 
set_app_var verdi_init_cmds "srcSetPreference -traceAddToWave off;"

set_app_var fml_default_task_alias SVA_FPV
read_file -top $top -cov all -format sverilog -sva -vcs "-cm_libs yv+celldefine -assert svaext -f fetch.filelist"
# LEARN model related settings
fvlearn_config -local_dir learn_data 

create_clock clk_i -period 100 -initial 1


#Reset settings 
create_reset rst_ni -low


set_change_at -clock {clk_i} -posedge -of_objects [all_input] 


fvassume {*prop_*}
fvassume {*prop_*}
fvassert {*prop_*_check*}



# run simulation phase for reset until the states of all regs is stable
sim_run -stable 

# force reset states to narrow scenario range and state space depth

# save the state after simulation phase is completed
# after this formal takes over from simulation
sim_save_reset

