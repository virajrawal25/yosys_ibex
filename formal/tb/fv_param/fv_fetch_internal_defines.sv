`define FV_CLKRESET_ON


`ifdef RISCV_FORMAL
  `define RVFI
`endif

`define FV_TB_ON

`define OP

`define OP_IMM
`define LUI
`define AUIPC

`define JAL
`define JALR

`define LOAD
`define STORE
/*
`define BRANCH
`define MISC
*/

`define RTL_TOP u_ibex_core
`define RTL_DECODE u_ibex_core.id_stage_i
`define RTL_FETCH u_ibex_core.if_stage_i
`define RTL_PREFETCH u_ibex_core.if_stage_i.gen_prefetch_buffer.prefetch_buffer_i
`define RTL_PRE_FIFO u_ibex_core.if_stage_i.gen_prefetch_buffer.prefetch_buffer_i.fifo_i
`define RTL_REG_FILE gen_regfile_ff.register_file_i
`define RTL_WRITE_BACK u_ibex_core.wb_stage_i


`define UNALLIGNED_JUMP_OFF


`ifdef FV_CLKRESET_ON
   `define FV_CLKRESET @(posedge clk_i) disable iff (~rst_ni)
`endif //FV_CLKRESET_ON

`ifdef FV_CLKRESET_ON
   `define FV_CLK @(posedge clk_i) 
`endif //FV_CLKRESET_ON


