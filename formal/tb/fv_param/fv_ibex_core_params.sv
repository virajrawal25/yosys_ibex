

//===========OPCODEs================//
localparam OP_OPCODE              =7'b110011;
localparam OP_IMM_OPCODE          =7'b10011;
localparam BRANCH_OPCODE          =7'b1100011;
localparam LUI_OPCODE             =7'b110111;
localparam AUIPC_OPCODE           =7'b10111;
localparam JAL_OPCODE             =7'b1101111;
localparam JALR_OPCODE            =7'b1100111;
localparam LOAD_OPCODE            =7'b11;
localparam STORE_OPCODE           =7'b100011;
localparam MISC_MEM_OPCODE        =7'b1111;


//===========Function3================//

//OP_OPCODE
localparam op_func3_ADD           =3'b0;
localparam op_func3_SUB           =3'b0;
localparam op_func3_SLL           =3'b1;
localparam op_func3_SLT           =3'b10;
localparam op_func3_SLTU          =3'b11;
localparam op_func3_XOR           =3'b100;
localparam op_func3_SRL           =3'b101;
localparam op_func3_SRA           =3'b101;
localparam op_func3_OR            =3'b110;
localparam op_func3_AND           =3'b111;

//OP_IMM_OPCODE
localparam op_imm_func3_ADDI      =3'b0;
localparam op_imm_func3_SLTI      =3'b10;
localparam op_imm_func3_SLTIU     =3'b11;
localparam op_imm_func3_XORI      =3'b100;
localparam op_imm_func3_ORI       =3'b110;
localparam op_imm_func3_ANDI      =3'b111;

//OP_IMM_OPCODE_SHIFT
localparam op_imm_func3_SLLI      =3'b1;
localparam op_imm_func3_SRLI      =3'b101;
localparam op_imm_func3_SRAI      =3'b101;

//BRANCH_OPCODE
localparam branch_func3_BEQ       =3'b0;
localparam branch_func3_BNE       =3'b1;
localparam branch_func3_BLT       =3'b100;
localparam branch_func3_BGE       =3'b101;
localparam branch_func3_BLTU      =3'b110;
localparam branch_func3_BGEU      =3'b111;

//JALR_OPCODE
localparam jalr_func3             =3'b0;

//LOAD_OPCODE
localparam load_func3_LB          =3'b0;
localparam load_func3_LH          =3'b1;
localparam load_func3_LW          =3'b10;
localparam load_func3_LBU         =3'b100;
localparam load_func3_LHU         =3'b101;

//STORE_OPCODE
localparam store_func3_SB         =3'b0;
localparam store_func3_SH         =3'b1;
localparam store_func3_SW         =3'b10;
localparam store_func3_SBU        =3'b100;
localparam store_func3_SHU        =3'b101;

//MISC_MEM_OPCODE
localparam misc_mem_func3_FENCE   =3'b0;
localparam misc_mem_func3_FENCE_I =3'b1;


//===========Function3================//
//OP_OPCODE
localparam op_func7_ADD           =7'b0;
localparam op_func7_SUB           =7'b100000;
localparam op_func7_SLL           =7'b0;
localparam op_func7_SLT           =7'b0;
localparam op_func7_SLTU          =7'b0;
localparam op_func7_XOR           =7'b0;
localparam op_func7_SRL           =7'b0;
localparam op_func7_SRA           =7'b100000;
localparam op_func7_OR            =7'b0;
localparam op_func7_AND           =7'b0;

localparam SHIFT_TYPE_LOGIC       =7'b0000000;
localparam SHIFT_TYPE_ARITH       =7'b0100000;


localparam NUM_REQS =2;
localparam int unsigned DEPTH = NUM_REQS+1;

localparam INST_W           =32;
localparam INST_OPCODE      =0;
localparam INST_OPCODE_W    =7;
localparam INST_FUNC3_W     =3;
localparam INST_FUNC7_W     =7;

